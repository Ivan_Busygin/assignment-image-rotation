#ifndef SOLUTION_ERROR_MESSAGES_H
#define SOLUTION_ERROR_MESSAGES_H

const char* const files_error_messages[] = {
  "Ошибка! Не существует нужный файл или каталог.\n",
  "Ошибка! Нет нужных прав.\n",
  "Какая-то ошибка при работе с файлом.\n"
};

const char* const bmp_error_messages[] = {
  "Ошибка! Содержимое файла не соответствует формату bmp.\n",
  "Какая-то ошибка при работе с форматом bmp.\n"
};

#endif
