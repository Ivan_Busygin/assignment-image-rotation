/* Преобразования изображений. */

#include "../include/transformations.h"

struct image rotate(struct image const source) {
    struct image result = create_image(source.height, source.width);
    for (uint32_t y = 0; y < source.height; y++)
        for (uint32_t x = 0; x < source.width; x++)
            *pixel_of(source.height - 1 - y, x, &result) = *pixel_of(x, y, &source);
    return result;
}
