/* Основная функция программы. */

#include <stdio.h>
#include <stdlib.h>

#include "../include/bmp.h"
#include "../include/error_messages.h"
#include "../include/files.h"
#include "../include/transformations.h"

void err(const char* message) {
  fputs(message, stderr);
  abort();
}

int main(int argc, char* argv[]) {
  if (argc < 3)
    err("Для выполнения программы необходимо два аргумента командной строки: "
        "файл исходного изображения и файл, который будет создан.\n");

  FILE* file;
  enum files_error_code files_code = open_file(&file, argv[1], "rb");
  if (files_code != FL_SUCCESS)
    err(files_error_messages[files_code]);

  struct image source = { 0 };
  enum bmp_error_code bmp_code = bmp_to_img(file, &source);
  fclose(file);
  if (bmp_code != BMP_SUCCESS) {
    destruct_image(&source);
    err(bmp_error_messages[bmp_code]);
  }

  struct image result = rotate(source);
  destruct_image(&source);

  files_code = open_file(&file, argv[2], "wb");
  if (files_code != FL_SUCCESS) {
    destruct_image(&result);
    err(files_error_messages[files_code]);
  }

  bmp_code = img_to_bmp(file, result);
  fclose(file);
  destruct_image(&result);
  if (bmp_code != BMP_SUCCESS)
    err(bmp_error_messages[bmp_code]);

  printf("Повёрнутая копия изображения успешно создана!\n");
  return 0;
}
